﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WallkeSeederWeb.Models.Foursquare;

namespace WallkeSeederWeb.App_Start
{
    public class WallkeProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<FoursquareApi.Models.Common.Venue.MiniVenue, MiniVenue>()
                .ForMember(dest=> dest._id, opt => opt.Ignore() )
                .ForMember(dest => dest.IdFoursquare, opt => opt.MapFrom( src => src.Id)).ReverseMap();
            Mapper.CreateMap<FoursquareApi.Models.Common.Category, Category>().ReverseMap();
            Mapper.CreateMap<FoursquareApi.Models.Common.Icon, Icon>().ReverseMap();
            Mapper.CreateMap<FoursquareApi.Models.Common.Contact, Contact>().ReverseMap();
            Mapper.CreateMap<FoursquareApi.Models.Common.Location, Location>().ReverseMap();
            Mapper.CreateMap<FoursquareApi.Models.Common.Price, Price>().ReverseMap();
            Mapper.CreateMap<FoursquareApi.Models.Common.Stats, Stats>().ReverseMap();  
        }

        public override string ProfileName
        {
            get { return this.GetType().Name; }
        } 
    }
}