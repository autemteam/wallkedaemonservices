﻿using AutoMapper;
using FoursquareApi;
using FoursquareApi.Models.Common.Venue;
using FoursquareApi.Models.Venues;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WallkeSeederWeb.Models.Foursquare;

namespace WallkeSeederWeb.Controllers
{
    public class SeedingController : Controller
    {        
        private const string ClientId = "JLMXUMUFIP5E4DBAS15W5JGAEOHMAYKXW5TOPI2N5V1VFKG4";//"0G504KQVCFU3OVYEGNKEK010TJ3BOTP2AZ120FT0YHVAOF0A";
        private const string ClientSecret = "CVNO33WCKUSBJRGSEZTELDPP3AI3XDGQLGSCGMSO3S3N1Y1J";// "5L43LXH5YMXJXU0MB35MXKDURA4KEQBP1ODUPRRCBPUR1IBB";
        private Foursquare foursquare = new Foursquare(ClientId, ClientSecret);
        private string RedirectUri = "/";
        private string AccessToken = "https://foursquare.com/oauth2/access_token";
        private static string connectionString = "mongodb://23.97.102.250:27017/Autem";
        private static MongoServer server = MongoServer.Create(connectionString);
        private static MongoDatabase db = server.GetDatabase("Autem");
        public const float RightDirection = 0.001000f;
        public const float DownDirection = 0.0010000f;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        //
        // GET: /Seeding/
        public ActionResult Foursquare()
        {
            return View();
        }

        //
        // GET: /Seeding/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // 
        // GET: /Seeding/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Seeding/Create
        [HttpPost]
        public JsonResult GetPlaces(FormCollection collection, double latitudeStart, double longitudeStart, double latitudeEnd, double longitudeEnd)
        {
            Dictionary<int, string> latLnDictionary = new Dictionary<int, string>();
            try
            {
                SearchParams searchInfo = CreateSearchParams(collection);
                db.GetCollection<Models.Foursquare.SearchParams>("SearchParams").Insert<SearchParams>(searchInfo);

                double startLat = searchInfo.LatitudeStart;
                double startLn = searchInfo.LongitudeStart;                            

                var idx = 0;
                while ( startLat >= searchInfo.LatitudeEnd   )
                {
                    latLnDictionary.Add(idx++, string.Format( "lat: {0} - ln: {1}", startLat, startLn ) );
                    var results = foursquare.Venues.SearchVenues(startLat, startLn, radius: 500);
                    var searchDetail = CreateSearchDetail(startLat, startLn, searchInfo._id);
                    db.GetCollection<Models.Foursquare.SearchParams>("SearchDetails").Insert<SearchDetail>(searchDetail);

                    if (results != null)
                    {
                        ProcessPlaces(results.Response.Venues);
                    }                    

                    if (startLn >= searchInfo.LongitudeEnd )
                    {
                        startLat -= DownDirection;
                        startLn = searchInfo.LongitudeStart;
                    }
                    else
                        startLn += RightDirection;
                }
                                
                return Json("OK");               
            }
            catch(Exception ex)
            {
                BsonDocument error = new BsonDocument();
                error.Add("errorMessage", ex.Message);
                error.Add("errorInnerExceptionMessage", ex.InnerException.Message);
                db.GetCollection("Errors").Insert(error);
                
                return Json(ex.Message.ToString());
            }
        }

        private SearchParams CreateSearchParams(FormCollection collection)
        {
            double latitudeStart = double.Parse(collection["latitudeStart"]);
            double longitudeStart = double.Parse(collection["longitudeStart"]);
            double latitudeEnd = double.Parse(collection["latitudeEnd"]);
            double longitudeEnd = double.Parse(collection["longitudeEnd"]);

            var searchInfo = new SearchParams()
            {
                LatitudeStart = latitudeStart,
                LongitudeStart = longitudeStart,
                LatitudeEnd = latitudeEnd,
                LongitudeEnd = longitudeEnd,
                Creation = DateTime.Now
            };            

            return searchInfo;
        }

        private SearchDetail CreateSearchDetail(double currentLat, double currentLn, ObjectId mainSearchId)
        {
            var searchDetail = new SearchDetail
            {
                CurrentDate = DateTime.Now,
                ParentSearch = mainSearchId,
                CurrentLat = currentLat,
                CurrentLn = currentLn
            };
            return searchDetail;            
        }

        private void ProcessPlaces(IEnumerable< FoursquareApi.Models.Common.Venue.CompactVenue> venues)
        {
            foreach (var venue in venues)
            {
                var venueWallke = Mapper.Map<FoursquareApi.Models.Common.Venue.MiniVenue, WallkeSeederWeb.Models.Foursquare.MiniVenue>(venue);

                if (!VenueExists(venue.Id))
                {
                    var collection = db.GetCollection<Models.Foursquare.MiniVenue>("FoursquarePlaces");
                    var result = collection.Insert<Models.Foursquare.MiniVenue>(venueWallke);
                }                
            }
        }

        public bool VenueExists(string idFoursquare) {
            var query = Query<Models.Foursquare.MiniVenue>.EQ(e => e.IdFoursquare, idFoursquare);
            var result = db.GetCollection<Models.Foursquare.MiniVenue>("FoursquarePlaces").FindOne(query);
            return result != null;
        }
        //
        // GET: /Seeding/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Seeding/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Seeding/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Seeding/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
