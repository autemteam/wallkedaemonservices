﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallkeSeederWeb.Models.Foursquare
{
    public class Category
    {
        public IEnumerable<Category> Categories { get; set; }
        public Icon Icon { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string PluralName { get; set; }
        public bool Primary { get; set; }
        public string ShortName { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
    }
}