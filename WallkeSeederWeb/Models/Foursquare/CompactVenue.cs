﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallkeSeederWeb.Models.Foursquare
{
    public class CompactVenue : MiniVenue
    {
        public Contact Contact { get; set; }
        public VenueMenu Menu { get; set; }
        public Price Price { get; set; }
        public short Rating { get; set; }
        public Stats Stats { get; set; }
        public string StoreId { get; set; }
        public string Url { get; set; }
        public bool Verified { get; set; }

    }
}