﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallkeSeederWeb.Models.Foursquare
{
    public class Contact
    {
        public string FormattedPhone { get; set; }
        public string Phone { get; set; }
        public string Twitter { get; set; }
    }
}