﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallkeSeederWeb.Models.Foursquare
{
    public class MiniVenue
    {
        public ObjectId _id { get; private set; }
        public IEnumerable<Category> Categories { get; set; }
        public string IdFoursquare { get; set; }
        public Location Location { get; set; }
        public string Name { get; set; }
    }
}