﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallkeSeederWeb.Models.Foursquare
{
    public class Price
    {
        public string Message { get; set; }
        public short Tier { get; set; }
    }
}