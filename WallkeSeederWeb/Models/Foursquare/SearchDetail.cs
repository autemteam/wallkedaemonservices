﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallkeSeederWeb.Models.Foursquare
{
    public class SearchDetail
    {
        public ObjectId _id { get; private set; }
        public ObjectId ParentSearch { get; set; }
        public double CurrentLat { get; set; }
        public double CurrentLn { get; set; }
        public DateTime CurrentDate { get; set; }
    }
}