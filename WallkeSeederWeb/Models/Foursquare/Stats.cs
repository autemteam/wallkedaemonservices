﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallkeSeederWeb.Models.Foursquare
{
    public class Stats
    {
        public int CheckinsCount { get; set; }
        public int? TipCount { get; set; }
        public int UsersCount { get; set; }
    }
}