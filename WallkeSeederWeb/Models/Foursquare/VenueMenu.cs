﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WallkeSeederWeb.Models.Foursquare
{
    public class VenueMenu
    {
        public string Anchor { get; set; }
        public string Label { get; set; }
        public string MobileUrl { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }
}