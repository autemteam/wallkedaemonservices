﻿$(document).ready(function () {
    
    $("form#foursquareParams").validate({
        rules: {
            "latitudeStart": {
                required: true
            },
            "longitudeStart": {
                required: true
            },            
            "latitudeEnd": {
                required: true
            },
            "longitudeEnd": {
                required: true
            }
        },
        //perform an AJAX post to ajax.php
        submitHandler: function () {
            
            $("#progressBar").show();
            $(".progress").show();
            $("#processInfo").hide();
           
            var startDate = getDateFormated(new Date());
            var textInfo = '';

            $.post("/Seeding/GetPlaces",
                $('form#foursquareParams').serialize(),
                function (data) {
                    var endDate = getDateFormated(new Date());
                    if (data == "OK") {
                        textInfo = 'Process end successfully. <br/>Start: ' + startDate + '<br/>End: ' + endDate;
                    }
                    else {
                        textInfo = 'Process end with errors. <br/>Start: ' + startDate + '<br/>End: ' + endDate + '<br/>';
                        textInfo += data;
                    }
                    $("#processInfo").html(textInfo);
                    $("#processInfo").show();
                    $(".progress").hide();
                });
        }
    });

    function getDateFormated(date) {        
        var day = date.getDate()
        var month = date.getMonth() + 1
        var year = date.getFullYear()
        return "<b>" + day + "/" + month + "/" + year + " - " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(); + "<b/>";
    }
});