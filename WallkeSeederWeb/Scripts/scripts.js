var events = null;
var map = null;
$(document).ready(function () {
    google.maps.event.addDomListener(window, 'load', initialize);

    getEvents();    
});


function setEvents() {
    for (var i = 0, tot = events.length; i < tot; i++) {
        createMarker(events[i], map, '<h3>' + events[i].Name + '</H3><p>' + events[i].Location.Address + ', ' + events[i].Location.Country + '</p>');
    }
}
function initialize() {
    var mapOptions = {
        zoom: 16
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            var markerOpt = {
                map: map,
                position: pos,
                title: 'Your location.',
                animation: google.maps.Animation.DROP
            };
            var googleMarker = new google.maps.Marker(markerOpt);


            map.setCenter(pos);
            setEvents();
        }, function () {
            handleNoGeolocation(true);
        });
    } else {
      
        handleNoGeolocation(false);
    }
}

function handleNoGeolocation(errorFlag) {
    if (errorFlag) {
        var content = 'Error: The Geolocation service failed.';
    } else {
        var content = 'Error: Your browser doesn\'t support geolocation.';
    }

    var options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
    };

    var infowindow = new google.maps.InfoWindow(options);
    map.setCenter(options.position);
}

function getEvents() {
    var request = $.ajax({
        url: "http://apiwallke.azurewebsites.net/api/foursquareVenues",
        type: "GET",
        dataType: "json",
        async: false
    });

    request.done(function (msg) {
        console.log(msg);
        events = msg;
    });

    request.fail(function (jqXHR, textStatus) {
        console.log("Request failed: " + textStatus);
    });
}

function createMarker(event, map, note) {
    console.log('Creando marcador: ' + event);
    var myLatlng = new google.maps.LatLng(parseFloat(event.Location.Lat), parseFloat(event.Location.Lng));
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: event.Name
    });
    marker.note = note;
    var info_window = new google.maps.InfoWindow({ content: '' });
    google.maps.event.addListener(marker, 'click', function () {
        info_window.content = this.note;
        info_window.open(this.getMap(), this);
    });
}