﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WallkeSeederWeb.Startup))]
namespace WallkeSeederWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
