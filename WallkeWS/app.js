﻿var logger = require('morgan');
var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT;

app.get('/', function (req, res) {
    res.send('Welcome to Wallke WS Service');
});

io.on('connection', function (socket) {
    console.log('a user connected: ' + socket.id);
    
    socket.on('messages', function (data) {
        console.log(data);
        io.emit('client', data);
    });
    socket.on('disconnect', function () {
        console.log(socket.name + ' has disconnected from Wallke.' + socket.id);
    });
    socket.on('error', function (err) {
        console.log(err);
    });
});

server.listen(port, function () {
    console.log('listening on *:' +81);
});
